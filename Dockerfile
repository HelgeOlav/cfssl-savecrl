FROM golang:alpine AS build
WORKDIR /src
copy . .
RUN go build

FROM alpine:latest AS bin
COPY --from=build /src/cfssl-savecrl /
RUN apk update && apk add ca-certificates

ENTRYPOINT ["/cfssl-savecrl"]
