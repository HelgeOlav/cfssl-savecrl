package main

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// This file handles configuration

// Parses the configuration
// Returns true if it is ok
// if printError is set it will show usage if there is any errors
func parseCmd(printError bool) bool {
	pflag.String("endpoint", "http://localhost:8888/api/v1/cfssl/crl", "full endpoint to connect to")
	pflag.String("saveto", "", "File to save to (has to be specified)")
	pflag.Int("sleep", 0, "Seconds to sleep between iterations, 0=disable")

	pflag.Parse()

	viper.BindPFlags(pflag.CommandLine)

	viper.SetEnvPrefix("CRL")
	viper.BindEnv("saveto")
	viper.BindEnv("endpoint")
	viper.BindEnv("sleep")

	SaveFile := viper.GetString("saveto")
	Sleep = viper.GetInt("sleep")

	if SaveFile == "" {
		if printError {
			pflag.Usage()
		}
		return false
	}
	return true
}
