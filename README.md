# Introduction

[CloudFlare's PKI/TLS toolkit](https://github.com/cloudflare/cfssl) is an extensive toolkit for working with certificates.
It is API and command-line oriented. What it does not have is a good way to work with CRL files directly.

This tool fetches the CRL using the API and saves it to a file on the filesystem.
This tool uses the API and is designed to be run remotely on the server that hosts the CRL.

If you are running everything locally you can to the same thing using a command like this:
```bash
cfssl crl -db-config db.json -ca file:ca.pem -ca-key file:ca-key.pem | base64 -d  > output.crl
```

# Usage

You only need to specify the endpoint for the crl and the output file you want to save to.

```
cfssl-savecrl --endpoint http://myserver:8888/api/v1/cfssl/crl --saveto /var/www/html/my.crl
```

This tool does not give any response if it succeded. In case of any failure it will print the error and also give an exit code of 1.

If you want it to run forever (not exiting in case of any errors) add ```--sleep #``` and it will run forever.
A value of 86400 makes it run once a day.

Parameters can also be specified as environment variables instead of command line args.

```
CRL_SLEEP=86400 CRL_ENDPOINT=http://myserver:8888/api/v1/cfssl/crl CRL_SAVETO=/var/www/html/my.crl cfssl-crl
```

# Docker

A Dockerfile is provided so you can build a container from the source code.

```bash
docker build --tag cfssl:latest .
```

# Amazon Lambda

This code can run serverless as a function in Amazon.
If you run it as a function compile it for Linux using ```GOOS=linux GOARCH=amd64 go build```,
zip it and upload the file. The function handler name is the name of the executable file.
When running as Lambda the ```--sleep``` is not supported.

For each run in Lambda you will get a JSON struct back of type Result.

# Remote filesystem support

This tool uses afero to handle different file systems. When ```-saveto``` uses a path & filename it is saved to the
local filesystem. To use other filesystems you need to construct it in form a URL like s3://bucket/object.

Only two remote filesystems are supported at this time. S3 and MemMapFs for testing.

memfs://filename stores the file to memory.

s3://bucket/object stores to file directly to S3. You should set the envionment varaible ```AWS_SDK_LOAD_CONFIG=true```
so it can load the region. Credentials are discovered using [automatic discovery](https://docs.aws.amazon.com/sdk-for-go/api/aws/session/).
 