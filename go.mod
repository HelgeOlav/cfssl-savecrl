module bitbucket.org/HelgeOlav/cfssl-savecrl

go 1.15

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/aws/aws-sdk-go v1.35.7
	github.com/fclairamb/afero-s3 v0.1.1
	github.com/spf13/afero v1.4.1
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
)
