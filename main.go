package main

import (
	"fmt"
	"os"
	"time"
)

var Sleep int // sleeptime between iterations in seconds

// CFSSL error and message struct
type CFSSLMessage struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// This is the JSON result we get back
type Result struct {
	Success  bool           `json:"success"`
	Result   []byte         `json:"result,omitempty"`
	Errors   []CFSSLMessage `json:"errors"`
	Messages []CFSSLMessage `json:"messages"`
}

// Check to exit program if we don't loop forever
type ErrorHandlerFunc func(e error)

var ErrorHandler ErrorHandlerFunc = func(e error) {
	if e == nil {
		return
	}
	fmt.Printf("%s: %v\n", time.Now().Format(time.RFC822), e)
	if Sleep <= 0 {
		os.Exit(1)
	}
}

// main
func main() {
	CheckAndRunAsLambda()        // see if this is a lambda function and move execution there
	if parseCmd(true) == false { // get command line parameters
		os.Exit(1)
	}
	for {
		req := new(InternalRequest)
		req.GetRequestParams()

		ErrorHandler(req.GetResponse())  // get response
		ErrorHandler(req.SaveResponse()) // save it
		if Sleep <= 0 {
			break
		}
		time.Sleep(time.Second * (time.Duration(Sleep)))
	} // for
}
