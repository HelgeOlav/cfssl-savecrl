package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
)

// This is a request object passed between getting the CRL and saving it
type InternalRequest struct {
	result       Result
	fs           afero.Fs
	FS_Savefile  string
	SaveFile     string `json:"saveto"`
	CRL_endpoint string `json:"endpoint"`
	Comment      string `json:"comment"`
}

// Get request parameters from command line and environment
func (i *InternalRequest) GetRequestParams() {
	i.SaveFile = viper.GetString("saveto")
	i.CRL_endpoint = viper.GetString("endpoint")

}

// This method fetches the response from the remote server
func (i *InternalRequest) GetResponse() error {
	i.result.Result = []byte{} // make empty response
	resp, err := http.Get(i.CRL_endpoint)
	if err != nil {
		return err
	}
	// get the body
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		msg := fmt.Sprintf("Fatal failure: When downloading %s, got %s", i.CRL_endpoint, resp.Status)
		return errors.New(msg)
	}
	err = json.Unmarshal(body, &i.result)
	if err != nil {
		msg := fmt.Sprintf("From %s, error when parsing JSON: %v", i.CRL_endpoint, err)
		return errors.New(msg)
	}
	if i.result.Success == false {
		msg := fmt.Sprintf("CFSSL returned server failure: %v", i.result.Errors)
		return errors.New(msg)
	}
	return nil
}
