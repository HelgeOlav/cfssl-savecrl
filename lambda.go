package main

import (
	"github.com/aws/aws-lambda-go/lambda"
	"os"
)

// This file adds support for running as AWS lambda

var LambdaOk bool

type LambdaRequest struct {
	SaveFile     string `json:"saveto"`
	CRL_endpoint string `json:"endpoint"`
	Comment      string `json:"comment"`
}

func CheckAndRunAsLambda() {
	resp := os.Getenv("AWS_LAMBDA_FUNCTION_NAME") // see if the AWS function name is defined
	if len(resp) == 0 {
		return
	}
	LambdaOk = parseCmd(false)
	lambda.Start(HandleLambdaEvent)
	// We should never return from here
	os.Exit(3)
}

func HandleLambdaEvent(param LambdaRequest) (result Result, _ error) {
	if LambdaOk == false {
		result.Messages = []CFSSLMessage{{
			Code:    510,
			Message: "Error from configuration, bailing out",
		}}
		return
	}
	req := new(InternalRequest)
	// Check request parameters
	req.GetRequestParams()
	if len(param.SaveFile) > 0 {
		req.SaveFile = param.SaveFile
	}
	if len(param.CRL_endpoint) > 0 {
		req.CRL_endpoint = param.CRL_endpoint
	}
	if len(param.Comment) > 0 {
		req.Comment = param.Comment
	}

	result.Messages = req.GetCurrentConfiguration()
	err := req.GetResponse()
	if err != nil {
		result.Errors = []CFSSLMessage{{
			Message: err.Error(),
			Code:    500,
		}}
		return
	} // err on GetResponse
	err = req.SaveResponse()
	if err != nil {
		result.Errors = []CFSSLMessage{{
			Message: err.Error(),
			Code:    501,
		}}
		return
	}
	result.Success = true
	return
}

func (i *InternalRequest) GetCurrentConfiguration() []CFSSLMessage {
	return []CFSSLMessage{{
		Code:    10,
		Message: i.CRL_endpoint,
	}, {
		Code:    11,
		Message: i.SaveFile,
	}, {
		Code:    12,
		Message: i.Comment,
	},
	}
}
