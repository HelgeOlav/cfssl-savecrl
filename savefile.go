package main

import (
	"errors"
	"fmt"
	session2 "github.com/aws/aws-sdk-go/aws/session"
	s3 "github.com/fclairamb/afero-s3"
	"github.com/spf13/afero"
	"strings"
)

// var fs afero.Fs
// var FS_Savefile string // used for afero filesystems

// try identify file system
func (i *InternalRequest) GetFileSystem() error {
	// check if filesystem is in place
	if i.fs != nil {
		return nil
	}
	// check if we should load a new filesystem. All but local file system is prefixed with type://
	params := strings.Split(i.SaveFile, "://")
	// see if we should load local file system
	if len(params) == 1 {
		i.fs = new(afero.OsFs)
		i.FS_Savefile = params[0]
		return nil
	}
	// now check what kind of savefile we have
	if len(params) == 2 {
		// check mem fs
		if params[0] == "memfs" {
			i.FS_Savefile = params[1]
			i.fs = new(afero.MemMapFs)
			return nil
		}
		if params[0] == "s3" {
			// fs = s3.NewFs("ddd", nil)
			var err error
			err = i.MakeS3Fs(params[1])
			return err
		}

	}
	return errors.New("Not supported filesystem")
}

// save response
func (i *InternalRequest) SaveResponse() error {
	if len(i.result.Result) > 0 {
		err := i.GetFileSystem()
		if err != nil {
			return err
		}
		err = afero.WriteFile(i.fs, i.FS_Savefile, i.result.Result, 0755)
		if err != nil {
			msg := fmt.Sprintf("Error when saving file: %v", err)
			return errors.New(msg)
		}
	}
	return nil
}

// Handler to load the S3 filesystem
// Full syntax for S3 is s3://bucket/filename
func (i *InternalRequest) MakeS3Fs(input string) error {
	bucket := strings.SplitN(input, "/", 2)
	if len(bucket) != 2 {
		return errors.New("Could not parse S3 bucket and name")
	}
	// try get a session
	session, err := session2.NewSession()
	if err != nil {
		return err
	}

	i.fs = s3.NewFs(bucket[0], session)
	i.FS_Savefile = bucket[1]
	return nil
}
